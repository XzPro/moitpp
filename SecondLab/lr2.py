from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
import random
#Определяем глобальные переменыые 
Clickable_btn = ['esc', 'enter']                                                         #список нажимаемых кнопок
operation=[('minus','-'), ('mult', '/'), ('plus','*')]                                   #список выполняемых операций

#Init webDriwer for FirefoX
driver = webdriver.Firefox()

url = "https://calc.by/math-calculators/scientific-calculator.html#!"                    #устанавливаем ссылку на сайт

#выбор случайной кнопки с цифровой клавиатуры
def random_numberButton():
    i=random.randint(1,9)
    return str(i)        


def calc():

   for o in operation:                                                                  #пока есть операции в списке
        Clickable_btn.insert(1,random_numberButton())                                   #нажимаем случайную кнопку с цифровой клавиатуры
        Clickable_btn.insert(2,o[0])                                                    #добавляем в список операцию
        Clickable_btn.insert(3,random_numberButton())                                   #нажимаем случайную кнопку с цифровой клавиатуры
    
        for l in Clickable_btn:                                                         #Пока в списке нажимаемых кнопок есть элементы
                driver.find_element_by_id("btn_"+l).click()                             #нажимаем на кнопку 
        result=str(eval(Clickable_btn[1]+o[1]+Clickable_btn[3]))                        #считаем результат
        if driver.find_element_by_id("calc_display_input").get_attribute("value")==result: #если результат на дисплее равен рассчитаному нами
                print('operation is valid '+ o[1])
        else:
                print ('operation is not valid'+ o[1])
        print(Clickable_btn[1]+ o[1]+Clickable_btn[3] +'= ',result)                     #выводим результаты на экран
        # Очищаем список
        Clickable_btn.pop(3)
        Clickable_btn.pop(2)  
        Clickable_btn.pop(1) 

#Проверка нажатий на цифры
def check(i):
        elem=driver.find_element_by_id("btn_"+i)
        elem.click()
        res_text = driver.find_element_by_id("calc_display_input").get_attribute("value")
        assert res_text==i, "ne"+i
        print(i)
        driver.find_element_by_id("btn_esc").click()

if __name__ == "__main__":
        driver.get(url)                                                                          #переходим по сссылке
        for i in range (10):
                check(str(i))
        calc()
        time.sleep(30)    
        driver.close()
