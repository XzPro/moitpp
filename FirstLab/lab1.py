from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
import random
#init global var`s
opened_tabs = []
sites = []
#Init webDriwer for FirefoX
browser = webdriver.Firefox()

#Define functions
def read_data_from_file():
    # performing file operations
    with open("C:/Users/Ivan/Desktop/MOiTPP/FirstLab/sites.txt","r") as f:
        for line in f.readlines():
            sites.append(line)

def random_open():
    count = len(sites)                                              #Determine the number of sites in the file
    for i in range(0,count):                                        #Create a list of N elements
        open = random.choice(sites)                                 #Select a random list item
        browser.get(open)                                           #Open the selected site
        opened_tabs.append(open)                                    #Add an open tab to the list of opened tabs
        sites.remove(open)                              #Delete an open site from the list of sites to be opened
        browser.execute_script("window.open()")                     #open new tab
        browser.switch_to.window(browser.window_handles[len(opened_tabs)]) #switch to the last open tab
    browser.close()                                                 #Close WebDriver
    print("All tabs were opened!")                                  #Displaying Text on the screen

# Close the tab
def close_all_tabs():
    print("Closing tabs")                                           #Displaying Text on the screen
    temp_list = opened_tabs.copy()
    temp_list.reverse()
    for site in reversed(opened_tabs):
    #for site in temp_list:
        #Close all tabs in reverse order.
        browser.switch_to.window(browser.window_handles[opened_tabs.index(site)])
        opened_tabs.remove(site)                                    #remove tab from list
        browser.close()                                             #Close WebDriver
        time.sleep(1)                                               #Sleep 1 sec
    print("All closed")

#Main function body
if __name__ == "__main__":
    #function call
    read_data_from_file()
    random_open()
    time.sleep(5)          
    close_all_tabs()